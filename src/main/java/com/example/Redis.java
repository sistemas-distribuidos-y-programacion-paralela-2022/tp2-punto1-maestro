package com.example;
        // Para evitar estos problemas se debe utilizar JedisPool, un pool de conexiones threadsafe. 
        // Utilizaremos el pool para crear de forma segura instancias de Jedis. 
        // De esta forma evitaremos errores y obtendremos un mejor rendimiento.
        // JedisPoolConfig incluye una serie de valores predeterminados útiles. 
        // Por ejemplo, si usamos Jedis con JedisPoolConfig se liberará la conexión tras 300 segundos (5 minutos) de inactividad.

import java.net.UnknownHostException;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

// La conexión a Redis la obtendremos con el método getResource() de JedisPoolConfig

        // Jedis jedis = pool.getResource();

        // Cuando no ya no necesitemos una conexión la cerraremos con close() y quedará liberada.

        // jedis.close();

        // Cuando hayamos finalizado el trabajo destruiremos el pool con destroy().

        // pool.destroy();
public class Redis {
    	// Threadsafe pool of network connections
	JedisPool pool;
	
	// Redis connection
	Jedis jedis;
	
	/**
	 * Returns a direct connection to a Redis database in localhost
	 * using the default port. 
	 *
	 * @return      a connection to Redis
	 * @see         Image
	 */
	public Jedis getDirectConnection() {
		jedis = new Jedis("127.0.0.1", 6379);
		
		return jedis;
	}
	
	/**
	 * Close a connection to a Redis database
	 *
	 */
	public void closeDirectConnection() {
		if (jedis != null) {
			jedis.close();
		}
	}
	
	/**
	 * Returns a connection from a pool to a Redis database in 
	 * localhost using the default port
	 * @param ipRedis
	 * 
	 * @return      a connection from a pool to Redis 
	 */
	public Jedis getConnection(String ipRedis) {
		pool = new JedisPool(new JedisPoolConfig(),ipRedis , 6379);		
		jedis = pool.getResource();
		return jedis;
		
		
	}
	
	/**
	 * Destroys a pool of network connections and close the connection 
	 *
	 */
	public void destroyPool() {

		// Close the connection
		if (jedis != null) {
			jedis.close();
		}
		
		// Destroy the pool
		if (pool != null) {
			pool.destroy();
		}
	}
    
}
