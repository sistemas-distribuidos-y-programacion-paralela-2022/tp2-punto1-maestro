package com.example;


import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.*;

public class ServerMaestro {
    // Se define el logger
    private final Logger log = LoggerFactory.getLogger(ServerMaestro.class);
    ServerSocket ss ; 
    int port;
    String ipRedis;

    //TODO cambiar los sys.err por logs
    public ServerMaestro() {
        try {
            Scanner scanner = new Scanner(System.in);
            port = Integer.valueOf(System.getenv("PUERTO_SERVIDOR"));     
            ipRedis = System.getenv("REDIS_SVC");            
            log.info("Configuracion del servidor Redis "+ipRedis+":puertoPorDefecto");       
            log.info("Conectando al servidor redis "+ipRedis+":puertoPorDefecto");
            this.ss = new ServerSocket(port);
            log.info ("El servidor se inicio en el puerto:  "+port);
            Socket client;
            Gson gson = new Gson();

            while (true) {

                client = ss.accept();
                // 1er paso
                ServerHilo sh = new ServerHilo(client, ipRedis);
                // 2do paso
                Thread serverThread = new Thread(sh);
                // 3er paso
                serverThread.start();
            }

        } catch (Exception e) {
            log.error("msg: "+e.getMessage());
        }

    }

    public static void main( String[] args )
    {
        ServerMaestro server = new ServerMaestro();
    }

}