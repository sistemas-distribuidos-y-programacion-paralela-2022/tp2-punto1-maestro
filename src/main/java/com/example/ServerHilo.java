package com.example;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Set;

import com.google.gson.Gson;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.Jedis;

public class ServerHilo implements Runnable {

    Integer sleepTime;
    private Socket client;
    private Message solicitud; 
    private final Logger log = LoggerFactory.getLogger(ServerMaestro.class);
    Gson gson = new Gson();
    private Redis redis;
    String ipRedis;

   public ServerHilo(Socket client, String ipRedis) {
        this.client = client;
        redis = new Redis(); //aplicar patron singleton
        this.ipRedis = ipRedis;
    }

    //Cuando se hace el .start()
    @Override
    public void run() {

        try {
            PrintWriter canalSalida = new PrintWriter (client.getOutputStream(), true);
            BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream()));
            String solicitudJson = br.readLine();
            System.err.println("Mensaje recibido: "+solicitudJson);
            Message solicitud = gson.fromJson(solicitudJson, Message.class);

            //El cleinte solicita el listado de archivos completo de todos los peers
            if (solicitud.getHeader().startsWith("getListadoCompleto")){
                log.info(" Se recibe "+solicitudJson);
                try{
                    //pedimos una conexion a Redis
                    log.info("intentando conectar a la bd redis en el puerto: "+ipRedis);
                    Jedis conx = redis.getConnection(ipRedis);
                    //obetenemos el listado completo pasando un patron
                    Set<String> consulta = conx.keys("*");
                    //envia la respuesta
                    String respuesta = gson.toJson(new Message ("getPerson", gson.toJson(consulta)));
                    canalSalida.println(respuesta);
                    log.info("se envia la siguiente respuesta: "+respuesta);

                }catch (Exception e){
                    log.error("No se obtuvo una solicitud valida o no se pudo conectar con Redis "+e.getMessage());
                }
            }

            if (solicitud.getHeader().startsWith("getListadoFiltrado")){
                log.info(" Se recibe un getListadoFiltrado");
                try{
                    //pedimos una conexion a Redis
                    Jedis conx = redis.getConnection(ipRedis);
                    //obetenemos el listado completo pasando un patron
                    Set<String> consulta = conx.keys(solicitud.getBodyJSON());
                    //envia la respuesta
                    String respuesta = gson.toJson(new Message ("200 OK", gson.toJson(consulta)));
                    canalSalida.println(respuesta);
                    log.info("se envia la siguiente respuesta: "+respuesta);


                }catch (Exception e){
                    log.error("No se recibio una Solicitud valida"+e.getMessage());
                }
            }

            //El cliente solicita actualizar el listado de archivos enviando un peer
            //Devuelve el listado actualizado
            if (solicitud.getHeader().startsWith("putArchivo")){
                log.info(" Se recibe un putArchivo ");
                try{
                    Archivo archivo = gson.fromJson(solicitud.getBodyJSON(), Archivo.class);
                    log.info("se recibe el body: "+archivo);

                    //se guarda en redis el archivo en formato json
                    Jedis conx = redis.getConnection(ipRedis);
                    conx.set(archivo.getFilename()+":"+archivo.getIp(), solicitud.getBodyJSON());
                    
                    //prueba de que se guardo bien
                    log.info("Con la siguiente clave "+archivo.getFilename()+archivo.getIp());
                    log.info("se guardo la siguiente tupla"+conx.get(archivo.getFilename()+archivo.getIp()));

                    //cierro conexion
                    conx.close();

                    //Se envia la respuesta
                    String respuesta = gson.toJson(new Message ("200 OK", ""));
                    canalSalida.println(respuesta);
                }catch (Exception e){
                    log.error("No se recibio una Solicitud valida"+e.getMessage());
                }
            }

            //Devuelve los metadatos del archivo que posee el filename guardado en el body
            if (solicitud.getHeader().startsWith("getMetadatosArchivo")){
                log.info(" Se recibe un getMetadatosArchivo");
                try{
                    String body = gson.fromJson(solicitud.getBodyJSON(), String.class);
                    log.info("BODY: "+body);

                    //se hace el pedido a redis
                    Jedis conx = redis.getConnection(ipRedis);
                    String archivoJson = conx.get(body);

                    //se cierra la conexion
                    conx.close();

                    //Se envia el archivo en formato json dentro de un mensaje en formato json jaja
                    String respuesta = gson.toJson(new Message ("200 OK", archivoJson));
                    canalSalida.println(respuesta);
                    log.info("se envia el siguiente mensaje: "+respuesta);
                }catch (Exception e){
                    log.error("No se recibio una Solicitud valida"+e.getMessage());
                }
            }

            if (solicitud.getHeader().startsWith("pruebaDeCarga1")){
                log.info(" Se recibe un pruebaDeCarga");
                try{
                    int i = 3;
                    String body = gson.fromJson(solicitud.getBodyJSON(), String.class);
                    log.info("BODY: "+body);

                    while(true){
                        i = i * i;
                        log.info("se calcula el numero "+i);
                        log.info("The results are specified by the language and independent of the JVM version: Integer.MAX_VALUE + 1 == Integer.MIN_VALUE and Integer.MIN_VALUE - 1 == Integer.MAX_VALUE. The same goes for the other integer types.");
                    }


                }catch (Exception e){
                    log.error("No se recibio una Solicitud valida"+e.getMessage());
                }
            }

            if (solicitud.getHeader().startsWith("pruebaDeCarga2")){
                log.info(" Se recibe un pruebaDeCarga");
                try{
                    int i = 3;
                    String body = gson.fromJson(solicitud.getBodyJSON(), String.class);
                    log.info("BODY: "+body);

                    while(i!=1){
                        i = i * i;
                        log.info("se calcula el numero "+i);
                        log.info("The results are specified by the language and independent of the JVM version: Integer.MAX_VALUE + 1 == Integer.MIN_VALUE and Integer.MIN_VALUE - 1 == Integer.MAX_VALUE. The same goes for the other integer types.");
                    }


                }catch (Exception e){
                    log.error("No se recibio una Solicitud valida"+e.getMessage());
                }
            }


            //posible mejora: dar de baja peers

            //Codigo comun a todas las respuestas
            //  Se hace flush y se registra la respuesta                
            // // force flush (variable = false)
            canalSalida.flush();
            log.info("Se respodio al puerto cliente: "+client.getPort() );
            
            // // Close current session
            canalSalida.close();
            br.close();
            client.close();

        }catch (Exception e) {
            log.info(" Nothing");
        }
    }
}
